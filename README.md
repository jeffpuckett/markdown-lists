The markdown list item [examples][1] are incorrect on Atlassian's site.

They show using 2-3 spaces for indented sub lists, but [4 spaces are required][2].

**Update:** This has [been fixed][3].

# Unordered list

* Item 1
* Item 2
* Item 3
  * Item 3a
  * Item 3b
  * Item 3c

# Ordered list

1. Step 1
2. Step 2
3. Step 3
   1. Step 3.1
   2. Step 3.2
   3. Step 3.3

# List in list

1. Step 1
2. Step 2
3. Step 3
   * Item 3a
   * Item 3b
   * Item 3c

----------------
# using 4 spaces
----------------

# Unordered list

* Item 1
* Item 2
* Item 3
    * Item 3a
    * Item 3b
    * Item 3c

# Ordered list

1. Step 1
2. Step 2
3. Step 3
    1. Step 3.1
    2. Step 3.2
    3. Step 3.3

# List in list

1. Step 1
2. Step 2
3. Step 3
    * Item 3a
    * Item 3b
    * Item 3c

[1]:https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html#Markdownsyntaxguide-Unorderedlist
[2]:http://stackoverflow.com/q/37575916/4233593
[3]:https://answers.atlassian.com/questions/38843984/
